package me.minex.delta.mods;

import org.lwjgl.input.Keyboard;

import me.minex.delta.main.Objects;

public class NoHurt {
	public void NoHurt() {
		if(!Keyboard.isKeyDown(Keyboard.KEY_N) == true){
			Objects.mc.thePlayer.capabilities.disableDamage = true;
		}else{
			if(Keyboard.isKeyDown(Keyboard.KEY_N)){
				Objects.mc.thePlayer.capabilities.disableDamage = false;
			}
		}
	}
}
