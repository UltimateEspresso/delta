package me.minex.delta;

public final class Delta {
	private final String NAME = "�4�l[�6�lDelta�l�4�l] ";
	public final String NAMETITLE = "Delta";
	public final String VERTITLE = "1.1.2_1 d";
	public final String MCVERTITLE = "1.7.x";
	private final String VER = " �e�l1.1.2_1 d";
	public final String MCVER = "�e�l1.7.x";
	public final String FLIGHT = "flight";
	
	public final String getName(){
		return this.NAME;
	}
	public final String getVer(){
		return this.VER;
	}
	public final String getMcVer(){
		return this.MCVER;
	}
	public final String getFli(){
		return this.FLIGHT;
	}
	public final String gettitlename(){
		return this.NAMETITLE;
	}
	public final String gettitlever(){
		return this.VERTITLE;
	}
	public final String gettitlemc(){
		return this.MCVERTITLE;
	}
}

